﻿namespace SampleSpreadsheet
{
    public class Constants
    {
        public const int DisplayWidthOfRowNo = 2;
        public const int DisplayWidthOfCell = 4;
        public const int MinCellValue = -99;
        public const int MaxCellValue = 999;
        public const int MinRowIndex = 0;
        public const int MaxRowIndex = 79;
        public const int MinColumnIndex = 0;
        public const int MaxColumnIndex = 59;

        public const string HelpText = @"
+------------------------------------------------------------------------------------+
|                       Command      Description                                     |
+------------------------------------------------------------------------------------+
| C w h                 | Should create a new spread sheet of width w and height h   |
|                       | (i.e. the spreadsheet can hold w * h amount of cells).     |
+------------------------------------------------------------------------------------+
| N x1 y1 v1            | Should insert a number in specified cell (x1,y1)           |
+------------------------------------------------------------------------------------+
| S x1 y1 x2 y2 x3 y3   | Should perform sum on top of all cells from x1 y1 to x2 y2 |
|                       | and store the result in x3 y3                              |
+------------------------------------------------------------------------------------+
| Q                     | Should quit the program.                                   |
+------------------------------------------------------------------------------------+
";
    }
}
