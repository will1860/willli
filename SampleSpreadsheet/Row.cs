﻿using System.Collections.Generic;

namespace SampleSpreadsheet
{
    public class Row<T>
    {
        private readonly List<ICell<T>> cells;

        public Row(List<ICell<T>> cells)
        {
            this.cells = cells;
        }

        public ICell<T> this[int column]
        {
            get { return cells[column]; }
            set { cells[column] = value; }
        }

        public ICell<T> this[Column column]
        {
            get { return cells[column.Index]; }
            set { cells[column.Index] = value; }
        }

        public int ColumnCount
        {
            get { return this.cells.Count; }
        }
    }
}
