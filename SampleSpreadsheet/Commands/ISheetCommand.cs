﻿namespace SampleSpreadsheet.Commands
{
    public interface ISheetCommand<TCellValue> : ICommand<ISpreadsheet<TCellValue>>
    {        
    }
}
