﻿using SampleSpreadsheet.Commands;
using System;

namespace SampleSpreadsheet
{
    public class QuitCommand : ICommand
    {
        public string Name => CommandName.OfQuitCommand;

        public void Excute(object[] args)
        {
            Environment.Exit(0);
        }
    }
}
