﻿using System;

namespace SampleSpreadsheet.Commands
{
    public class HelpCommand : ICommand
    {
        public string Name => CommandName.OfHelpCommand;

        public void Excute(object[] args)
        {
            Console.WriteLine(Constants.HelpText);
        }
    }
}
