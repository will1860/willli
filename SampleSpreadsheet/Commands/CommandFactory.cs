﻿using System;

namespace SampleSpreadsheet.Commands
{
    public static class CommandFactory
    {
        public static ISheetCommand<int> CreateSheetCommand(string name)
        {
            switch (name)
            {
                case "C":
                    return new CreateSheetCommand();
                case "N":
                    return new EditCellCommand();
                case "S":
                    return new SumCommand();
                default:
                    throw new NotSupportedException($"'{name}' command is not support yet.");

            }
        }

        public static ICommand CreateGeneralCommand(string name)
        {
            switch (name)
            {
                case "H":
                case "?":
                    return new HelpCommand();
                case "Q":
                    return new QuitCommand();
                default:
                    return CreateSheetCommand(name);
            }
        }
    }
}
