﻿namespace SampleSpreadsheet
{
    public interface ICommand
    {
        string Name { get; }
        void Excute(object[] args);
    }

    public interface ICommand<TResult> : ICommand
    {
        TResult Result { get; }
    }
}