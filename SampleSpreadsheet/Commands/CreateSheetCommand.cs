﻿using SampleSpreadsheet.Arguments;
using SampleSpreadsheet.Commands;
using System.Collections.Generic;

namespace SampleSpreadsheet
{
    public class CreateSheetCommand : ISheetCommand<int>
    {
        private const int NumberOfArguments = 3;
        private static readonly CreateSheetArgumentParser argsParser;

        static CreateSheetCommand()
        {
            argsParser = new CreateSheetArgumentParser();
        }

        public string Name => CommandName.OfCreateSheetCommand;

        public ISpreadsheet<int> Result { get; private set; }

        public void Excute(object[] args)
        {
            var spreadsheet = new Spreadsheet<int>();
            (int col, int row) = argsParser.Parse(args);

            // add columns
            for (int i = 0; i < col; i++)
            {
                spreadsheet.AddColumn();
            }

            // add rows
            for (int i = 0; i < row; i++)
            {
                var numberCells = new List<ICell<int>>();

                for (int j = 0; j < col; j++)
                {
                    numberCells.Add(new NumberCell(i, j, 0));
                }

                spreadsheet.AddRow(numberCells);
            }

            this.Result = spreadsheet;
        }
    }
}
