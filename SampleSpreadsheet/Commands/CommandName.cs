﻿namespace SampleSpreadsheet.Commands
{
    public class CommandName
    {
        public const string OfCreateSheetCommand = "C";
        public const string OfEditCellCommand = "N";
        public const string OfSumCommand = "S";
        public const string OfQuitCommand = "Q";
        public const string OfHelpCommand = "H";
    }
}