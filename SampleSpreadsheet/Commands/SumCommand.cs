﻿using SampleSpreadsheet.Arguments;
using SampleSpreadsheet.Commands;

namespace SampleSpreadsheet
{
    public class SumCommand : ISheetCommand<int>
    {
        public string Name => CommandName.OfSumCommand;

        public ISpreadsheet<int> Result { get; private set; }

        public void Excute(object[] args)
        {
            var indexParser = new IndexParser();
            var spreadsheet = (ISpreadsheet<int>)args[0];
            indexParser.TryParse(args[2], out int startRow);
            indexParser.TryParse(args[1], out int startColumn);
            indexParser.TryParse(args[4], out int endRow);
            indexParser.TryParse(args[3], out int endColumn);
            indexParser.TryParse(args[6], out int resultRow);
            indexParser.TryParse(args[5], out int resultColumn);

            var sum = spreadsheet.SumOfCells(startRow - 1, startColumn - 1, endRow - 1, endColumn - 1,
                (firstValue, secondValue) => firstValue + secondValue);

            spreadsheet.ModifyCellValue(resultRow - 1, resultColumn - 1, sum);

            this.Result = spreadsheet;
        }
    }
}
