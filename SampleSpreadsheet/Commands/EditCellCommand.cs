﻿using SampleSpreadsheet.Arguments;
using SampleSpreadsheet.Commands;
using System;

namespace SampleSpreadsheet
{
    public class EditCellCommand : ISheetCommand<int>
    {
        private const int NumberOfArguments = 4;

        public string Name => CommandName.OfEditCellCommand;

        public ISpreadsheet<int> Result { get; private set; }

        public void Excute(object[] args)
        {
            ParseArguments(args, out int row, out int col, out int value, out ISpreadsheet<int> spreadsheet);
            spreadsheet.Rows[row - 1][col - 1].ModifyValue(value);

            this.Result = spreadsheet;
        }

        private void ParseArguments(object[] args, out int row, out int col, out int value, out ISpreadsheet<int> spreadsheet)
        {
            row = -1;
            col = -1;
            value = 0;
            spreadsheet = null;

            if (args == null)
                throw new ArgumentNullException(nameof(args), "please enter invalid parameters.");

            if (args.Length < NumberOfArguments)
                throw new ArgumentException($"please enter {NumberOfArguments} parameters", nameof(args));

            var indexParser = new IndexParser();

            spreadsheet = args[0] as ISpreadsheet<int>;
            if (spreadsheet == null)
                throw new ArgumentException("the first parameter is not a valid spreadsheet", "args[0]");

            if (!indexParser.TryParse(args[2], out row))
                throw new ArgumentException("please enter a valid row", "arg[2]");

            if (!indexParser.TryParse(args[1], out col))
                throw new ArgumentException("please enter a valid column", "args[1]");

            if (!indexParser.TryParse(args[3], out value))
                throw new ArgumentException("please enter a valid value", "args[3]");
        }
    }
}