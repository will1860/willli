﻿using SampleSpreadsheet.Arguments;
using System;

namespace SampleSpreadsheet
{
    public class NumberCell : ICell<int>
    {
        private static readonly string InvalidValueErrorMessage = $"the value of cell should be between {Constants.MinCellValue} and {Constants.MaxCellValue}";
        private static readonly string InvalidRowErrorMessge = $"the row of cell should be between {Constants.MinRowIndex + 1} and {Constants.MaxRowIndex + 1}";
        private static readonly string InvalidColumnErrorMessge = $"the clumn of cell should be between {Constants.MinColumnIndex + 1} and {Constants.MaxRowIndex + 1}";

        public NumberCell(int rowIndex, int columnIndex, int value)
        {
            var rowIndexValidator = new IndexValidator(Constants.MinRowIndex, Constants.MaxRowIndex);
            var columnIndexValidator = new IndexValidator(Constants.MinColumnIndex, Constants.MaxColumnIndex);

            if (!IsValidValue(value))
                throw new ArgumentException(nameof(value), InvalidValueErrorMessage);

            if (!rowIndexValidator.IsValidIndex(rowIndex))
                throw new ArgumentException(nameof(rowIndex), InvalidRowErrorMessge);

            if (!columnIndexValidator.IsValidIndex(columnIndex))
                throw new ArgumentException(nameof(columnIndex), InvalidColumnErrorMessge);

            this.RowIndex = rowIndex;
            this.ColumnIndex = columnIndex;
            this.Value = value;
        }

        public int Value { get; private set; }

        public int RowIndex { get; private set; }

        public int ColumnIndex { get; private set; }

        public void ModifyValue(int newValue)
        {
            if (!IsValidValue(newValue))
                throw new ArgumentException(nameof(newValue), InvalidValueErrorMessage);

            this.Value = newValue;
        }

        private bool IsValidValue(int value)
        {
            return Constants.MinCellValue <= value && value <= Constants.MaxCellValue;
        }
    }
}
