﻿namespace SampleSpreadsheet
{
    public class Column
    {
        public Column(int index)
        {
            this.Index = index;
        }

        public int Index { get; private set; }
    }
}
