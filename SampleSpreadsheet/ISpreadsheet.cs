﻿using System;
using System.Collections.Generic;

namespace SampleSpreadsheet
{
    public interface ISpreadsheet<T>
    {
        List<Row<T>> Rows { get; }

        List<Column> Columns { get; }

        Column AddColumn();

        Row<T> AddRow(List<ICell<T>> cells);

        void ModifyCellValue(int rowIndex, int columnIndex, T newValue);

        T SumOfCells(int startRowIndex, int startColumnIndex, int endRowIndex, int endColumnIndex, Func<T, T, T> add);
    }
}
