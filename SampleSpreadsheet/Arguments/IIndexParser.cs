﻿namespace SampleSpreadsheet.Arguments
{
    public interface IIndexParser
    {
        bool TryParse(object input, out int result);
        bool TryParse(string input, out int result);
    }
}
