﻿namespace SampleSpreadsheet.Arguments
{
    public interface IIndexValidator
    {
        bool IsValidIndex(int index);
    }
}
