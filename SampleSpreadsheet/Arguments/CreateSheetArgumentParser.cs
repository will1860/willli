﻿using System;

namespace SampleSpreadsheet.Arguments
{
    public class CreateSheetArgumentParser
    {
        private const int NumberOfArguments = 3;
        private static readonly IIndexParser indexParser;
        private static readonly IIndexValidator rowIndexValidator;
        private static readonly IIndexValidator columnIndexValidator;

        static CreateSheetArgumentParser()
        {
            indexParser = new IndexParser();
            rowIndexValidator = new IndexValidator(Constants.MinRowIndex, Constants.MaxRowIndex);
            columnIndexValidator = new IndexValidator(Constants.MinColumnIndex, Constants.MaxColumnIndex);
        }

        public (int width, int height) Parse(params object[] args)
        {
            // args[0]: command name
            // args[1]: width
            // args[2]: height

            var row = -1;
            var col = -1;

            if (args == null)
                throw new ArgumentNullException("please enter parameters", nameof(args));

            if (args.Length < NumberOfArguments)
                throw new ArgumentException($"please enter {NumberOfArguments} parameter(s).", nameof(args));

            if (!indexParser.TryParse(args[1], out col))
                throw new ArgumentException("please enter a valid column", "args[1]");

            if (!indexParser.TryParse(args[2], out row))
                throw new ArgumentException("please enter a valid row", "args[2]");

            if (!rowIndexValidator.IsValidIndex(row - 1))
                throw new ArgumentOutOfRangeException("height", $"please enter a valid 'height' between 1 and {Constants.MaxRowIndex + 1}");

            if (!columnIndexValidator.IsValidIndex(col - 1))
                throw new ArgumentOutOfRangeException("width", $"please enter a valid 'width' between 1 and {Constants.MaxColumnIndex + 1}");

            return (width: col, height: row);
        }
    }
}
