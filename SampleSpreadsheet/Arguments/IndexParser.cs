﻿namespace SampleSpreadsheet.Arguments
{
    public class IndexParser : IIndexParser
    {
        public bool TryParse(object input, out int result)
        {
            return TryParse(input?.ToString(), out result);
        }

        public bool TryParse(string input, out int result)
        {
            if (string.IsNullOrEmpty(input))
            {
                result = -1;

                return false;
            }

            if (!int.TryParse(input, out result))
            {
                result = -1;

                return false;
            }

            return true;
        }
    }
}