﻿namespace SampleSpreadsheet.Arguments
{
    public class IndexValidator : IIndexValidator
    {
        private readonly int lowBound;
        private readonly int highBound;

        public IndexValidator(int lowBound, int highBound)
        {
            this.lowBound = lowBound;
            this.highBound = highBound;
        }

        public bool IsValidIndex(int index)
        {
            return lowBound <= index && index <= highBound;
        }
    }
}