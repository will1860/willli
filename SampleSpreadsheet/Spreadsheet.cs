﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleSpreadsheet
{
    public class Spreadsheet<T> : ISpreadsheet<T>
    {
        public Spreadsheet()
        {
            this.Rows = new List<Row<T>>();
            this.Columns = new List<Column>();
        }

        public List<Row<T>> Rows { get; private set; }

        public List<Column> Columns { get; private set; }

        public Column AddColumn()
        {
            var colIndex = Columns.Count;
            var column = new Column(colIndex);
            this.Columns.Add(column);

            return column;
        }

        public Row<T> AddRow(List<ICell<T>> cells)
        {
            var row = new Row<T>(cells);
            this.Rows.Add(row);

            return row;
        }

        public void ModifyCellValue(int rowIndex, int columnIndex, T newValue)
        {
            this.Rows[rowIndex][columnIndex].ModifyValue(newValue);
        }

        public T SumOfCells(int startRowIndex, int startColumnIndex, int endRowIndex, int endColumnIndex, Func<T, T, T> add)
        {
            T sum = default(T);

            for (int rowIndex = startRowIndex; rowIndex <= endRowIndex; rowIndex++)
            {
                for (int colIndex = startColumnIndex; colIndex <= endColumnIndex; colIndex++)
                {
                    sum = add(sum, this.Rows[rowIndex][colIndex].Value);
                }
            }

            return sum;
        }

        public override string ToString()
        {
            var topBorderChar = '-';
            var leftBorderChar = '|';
            var cornerChar = '*';
            var topBorderWidth = this.Columns.Count * Constants.DisplayWidthOfCell;

            // for example:
            //
            //       1   2   3
            //   *------------*
            //  1|   0   0   0|
            //  2|   0   0   0|
            //   *------------*
            var topBorder = new string(' ', Constants.DisplayWidthOfRowNo) + cornerChar + new string(topBorderChar, topBorderWidth) + cornerChar;
            var displayFormatOfCell = $"{{0,{Constants.DisplayWidthOfCell}}}";
            var displayFormatOfRowNo = $"{{0,{Constants.DisplayWidthOfRowNo}}}";
            var sb = new StringBuilder();

            // header
            sb.Append(new string(' ', Constants.DisplayWidthOfRowNo + 1));

            for (int i = 0; i < this.Columns.Count; i++)
            {
                sb.AppendFormat(displayFormatOfCell, i + 1); // column number(column index + 1)
            }

            // top border
            sb.AppendLine();
            sb.AppendLine(topBorder);

            // body
            int rowNo = 1;
            foreach (var row in this.Rows)
            {
                sb.AppendFormat(displayFormatOfRowNo, rowNo.ToString()); // start with row number
                sb.Append(leftBorderChar); // left border char

                foreach (var col in this.Columns)
                {
                    sb.AppendFormat(displayFormatOfCell, row[col].Value.ToString());
                }

                sb.Append(leftBorderChar); // end with right border char          
                sb.AppendLine(); // new row

                ++rowNo;
            }

            // bottom border
            sb.Append(topBorder);

            return sb.ToString();
        }
    }
}
