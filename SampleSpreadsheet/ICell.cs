﻿namespace SampleSpreadsheet
{
    public interface ICell<T>
    {
        T Value { get; }

        int RowIndex { get; }

        int ColumnIndex { get; }

        void ModifyValue(T newValue);
    }
}
