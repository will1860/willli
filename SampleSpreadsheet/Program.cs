﻿using SampleSpreadsheet.Commands;
using System;

namespace SampleSpreadsheet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Simple Spreadsheet");
            Console.WriteLine("enter command: H or ? to look up available commands.");

            var commandText = Console.ReadLine();
            ISpreadsheet<int> spreadsheet = null;

            while (!string.IsNullOrEmpty(commandText))
            {
                try
                {
                    // prepare command
                    var parameters = commandText.Split(' ');
                    var commandName = parameters[0];
                    var command = CommandFactory.CreateGeneralCommand(commandName);

                    // excute command
                    if (command is ISheetCommand<int> sheetCommand)
                    {
                        var parameters2 = new object[parameters.Length];
                        Array.Copy(parameters, parameters2, parameters.Length);
                        parameters2[0] = spreadsheet;
                        sheetCommand.Excute(parameters2);
                        spreadsheet = sheetCommand.Result;

                        // display spreadsheet
                        Console.WriteLine(spreadsheet.ToString());
                    }
                    else if(command != null)
                    {
                        command.Excute(null);
                    }
                }
                catch (NotSupportedException nse)
                {
                    Console.WriteLine(nse.Message);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                // enter command
                Console.WriteLine("enter command:");
                commandText = Console.ReadLine();
            }
        }
    }
}
