# Simple Spreadsheet Console Application

This is a simple spreadsheet console application created by Will Li

## Design Docs
- Introduction
    - Project structure:
        - SampleSpreadsheet
        - ----Arguments (arguments parsers & validators)
        - ----Commands (commands interfaces & implementations)
        - SampleSpreadsheet.Tests
        - ----Commands
- Architectural Principles

## Running the sample
1. Open the solution file WillLi.sln by Visual Studio 2017 or above.

2. Press Ctrl+F5 to run the app.

## Running the tests

1. Open the solution by Visual Studio 2017 or above.

2. Open Tests->Window->Test Explorer-->Run All