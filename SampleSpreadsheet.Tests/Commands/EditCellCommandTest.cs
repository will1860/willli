﻿using SampleSpreadsheet.Commands;
using System;
using Xunit;

namespace SampleSpreadsheet.Tests.Commands
{
    public class EditCellCommandTest
    {
        [Fact]
        public void Name()
        {
            // Act
            var command = new EditCellCommand();

            // Assert
            Assert.Equal(CommandName.OfEditCellCommand, command.Name);
        }

        [Fact]
        public void Execute()
        {
            // Arrange
            var col = 1;
            var row = 2;
            var newValue = 2;

            var width = 20;
            var height = 4;
            var spreadsheet = CreateSpreadsheet(width, height);
            var args = new object[] { spreadsheet, col, row, newValue };
            var command = CommandFactory.CreateSheetCommand(CommandName.OfEditCellCommand);

            // Act
            command.Excute(args);
            var result = command.Result;

            // Assert
            Assert.NotNull(result);
            Assert.Same(spreadsheet, result);
            Assert.Equal(newValue, spreadsheet.Rows[row - 1][col - 1].Value);
            for (int rowIndex = 0; rowIndex < height; rowIndex++)
            {
                for (int colIndex = 0; colIndex < width; colIndex++)
                {
                    if (rowIndex == row - 1 && colIndex == col - 1)
                        continue;

                    // make sure that the rest of cell arn't changed.
                    Assert.Equal(0, spreadsheet.Rows[rowIndex][colIndex].Value);
                }
            }
        }

        [Fact]
        public void Excute_WithNullArguments()
        {
            // Arrange
            object[] args = null;
            var command = CommandFactory.CreateSheetCommand(CommandName.OfEditCellCommand);

            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => command.Excute(args));
        }

        [Theory]
        [InlineData(1)]
        [InlineData(1, 2)]
        [InlineData(1, 2, 3)]
        public void Execute_WithIncorrectNumberOfArguments(params object[] args)
        {
            // Arrange
            var command = CommandFactory.CreateSheetCommand(CommandName.OfEditCellCommand);

            // Act & Assert
            Assert.Throws<ArgumentException>(() => command.Excute(args));
        }

        public static TheoryData<object[]> TestDataOfExecute_WithInvalidArguments =>
            new TheoryData<object[]>
            {
                new object[]{ "", "not a number", 1, 2 },
                new object[]{ CreateSpreadsheet(20, 4), "not a number", 1, 2 },
                new object[]{ CreateSpreadsheet(20, 4), 1, "not a number", 2 },
                new object[]{ CreateSpreadsheet(20, 4), 1, 2, "not a number" },
            };

        [Theory]
        [MemberData(nameof(TestDataOfExecute_WithInvalidArguments))]
        public void Execute_WithInvalidArguments(params object[] args)
        {
            // Arrange
            var command = CommandFactory.CreateSheetCommand(CommandName.OfEditCellCommand);

            // Act & Assert
            Assert.Throws<ArgumentException>(() => command.Excute(args));
        }

        private static ISpreadsheet<int> CreateSpreadsheet(int width, int height)
        {
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);
            var args = new object[] { CommandName.OfCreateSheetCommand, width, height };
            command.Excute(args);

            return command.Result;
        }
    }
}
