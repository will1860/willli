﻿using SampleSpreadsheet.Commands;
using System;
using Xunit;

namespace SampleSpreadsheet.Tests.Commands
{
    public class CreateSheetCommandTest
    {
        [Fact]
        public void Name()
        {
            // Arrange
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);

            // Assert
            Assert.NotNull(command);
            Assert.Equal(CommandName.OfCreateSheetCommand, command.Name);
        }

        [Theory]
        [InlineData(20, 4)]
        [InlineData(60, 80)]
        public void Excute(int width, int height)
        {
            // Arrange
            var args = new object[] { CommandName.OfCreateSheetCommand, width, height };
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);

            // Act
            command.Excute(args);
            var spreadsheet = command.Result;

            // Assert
            Assert.NotNull(spreadsheet);
            Assert.IsAssignableFrom<ISpreadsheet<int>>(spreadsheet);
            Assert.Equal(width, spreadsheet.Columns.Count);
            Assert.Equal(height, spreadsheet.Rows.Count);
            foreach (var row in spreadsheet.Rows)
            {
                foreach (var col in spreadsheet.Columns)
                {
                    Assert.Equal(0, row[col].Value);
                }
            }
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(-1, 0)]
        [InlineData(0, -1)]
        [InlineData(61, 80)]
        [InlineData(60, 81)]
        [InlineData(int.MaxValue, int.MaxValue)]
        [InlineData(int.MinValue, int .MinValue)]
        public void Excute_ThrowOutOfRangeException(int width, int height)
        {
            // Arrange
            var args = new object[] { CommandName.OfCreateSheetCommand, width, height };
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);

            // Assert
            Assert.Throws<ArgumentOutOfRangeException>(() => command.Excute(args));
        }

        [Fact]
        public void Excute_WithNullArguments()
        {
            // Arrange
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);
            object[] args = null;

            // Act
            Assert.Throws<ArgumentNullException>(() => command.Excute(args));
        }

        [Fact]
        public void Excute_WithIncorrectNumberOfArguments()
        {
            // Arrange
            var width = 20;
            var height = 4;
            var args = new object[] { width, height };
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);

            // Act
            Assert.Throws<ArgumentException>(() => command.Excute(args));
        }

        [Theory]
        [InlineData(null, "not a number", 1)]
        [InlineData("C", 1, "not a number")]
        public void Excute_WithInvalidArguments(params object[] args)
        {
            // Arrange
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);

            // Act
            Assert.Throws<ArgumentException>(() => command.Excute(args));
        }
    }
}
