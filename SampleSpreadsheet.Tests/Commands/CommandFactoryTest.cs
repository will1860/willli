﻿using SampleSpreadsheet.Commands;
using System;
using System.Diagnostics;
using System.Linq;
using Xunit;

namespace SampleSpreadsheet.Tests.Commands
{
    public class CommandFactoryTest
    {
        [Theory]
        [InlineData(CommandName.OfCreateSheetCommand, typeof(CreateSheetCommand))]
        [InlineData(CommandName.OfEditCellCommand, typeof(EditCellCommand))]
        [InlineData(CommandName.OfSumCommand, typeof(SumCommand))]
        public void CreateCreateSheetCommand(string commandName, Type expectedCommandType)
        {
            // Arrange & Act
            var command = CommandFactory.CreateSheetCommand(commandName);

            // Assert
            Assert.NotNull(command);
            Assert.IsType(expectedCommandType, command);
        }

        [Fact]
        public void CreateCommand_ThrowNotSupportedException()
        {
            // Arrange
            var commandName = GenerateCommandNameExcludedFromAvailabe();

            // Assert
            Assert.Throws<NotSupportedException>(() => CommandFactory.CreateSheetCommand(commandName));
        }

        private string GenerateCommandNameExcludedFromAvailabe()
        {
            var availableCommandNames = new string[]
            {
                CommandName.OfCreateSheetCommand,
                CommandName.OfEditCellCommand,
                CommandName.OfSumCommand,
            };

            var commandName = string.Empty; // empty string also isn't an avaialbe command name.
            var upperChars = "ABCDEFGHIJKLUMNOPQRSTUVWXYZ";

            var random = new Random();
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var startTime = stopwatch.Elapsed;
            var timeout = TimeSpan.FromSeconds(10);

            while (string.IsNullOrEmpty(commandName)
                || availableCommandNames.Contains(commandName)
                || (stopwatch.Elapsed - startTime) > timeout)
            {
                var index = random.Next(0, upperChars.Length);
                commandName = new string(upperChars[index], 1);
            }

            stopwatch.Stop();

            return commandName;
        }
    }
}
