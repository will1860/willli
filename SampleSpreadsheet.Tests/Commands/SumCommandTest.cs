﻿using SampleSpreadsheet.Commands;
using System;
using Xunit;

namespace SampleSpreadsheet.Tests.Commands
{
    public class SumCommandTest
    {
        [Fact]
        public void Name()
        {
            // Act
            var command = new SumCommand();

            // Assert
            Assert.Equal(CommandName.OfSumCommand, command.Name);
        }

        // width, height, startColumn, startRow, endColumn, endRow, resultColumn, resultRow
        public static TheoryData<int, int, int, int, int, int, int, int> TestDataOfExcute =>
            new TheoryData<int, int, int, int, int, int, int, int>
            {
                { 20, 8, 1, 2, 1, 3, 1, 4 },
                { 20, 8, 1, 1, 3, 3, 1, 4 },
                { 20, 8, 2, 2, 5, 5, 2, 6 },
            };

        [Theory]
        [MemberData(nameof(TestDataOfExcute))]
        public void Excute(int width, int height, int startColumn, int startRow, int endColumn, int endRow, int resultColumn, int resultRow)
        {
            // Arrange
            var spreadsheet = CreateSpreadsheet(width, height);
            var sum = 0;
            var random = new Random();

            for (int rowIndex = startRow - 1; rowIndex < endRow; rowIndex++)
            {
                for (int colIndex = startColumn - 1; colIndex < endColumn; colIndex++)
                {
                    var randomValue = random.Next(1, 30);
                    spreadsheet.Rows[rowIndex][colIndex].ModifyValue(randomValue);
                    sum += randomValue;
                }
            }
            
            var args = new object[] { spreadsheet, startColumn, startRow, endColumn, endRow, resultColumn, resultRow };
            var command = CommandFactory.CreateSheetCommand(CommandName.OfSumCommand);

            // Act
            command.Excute(args);
            var result = command.Result;

            // Assert
            Assert.NotNull(result);
            Assert.IsAssignableFrom<ISpreadsheet<int>>(result);
            Assert.Equal(sum, spreadsheet.Rows[resultRow - 1][resultColumn - 1].Value);
        }

        private static ISpreadsheet<int> CreateSpreadsheet(int width, int height)
        {
            var command = CommandFactory.CreateSheetCommand(CommandName.OfCreateSheetCommand);
            var args = new object[] { CommandName.OfCreateSheetCommand, width, height };
            command.Excute(args);

            return command.Result;
        }
    }
}
