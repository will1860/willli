using System;
using Xunit;

namespace SampleSpreadsheet.Tests
{
    public class NumberCellTest
    {
        [Theory]
        [InlineData(0, 0, -99)]
        [InlineData(79, 59, 999)]
        public void NewNumberCell(int row, int column, int value)
        {
            // Arrange
            var cell = new NumberCell(row, column, value);

            // Assert
            Assert.NotNull(cell);
            Assert.Equal(row, cell.RowIndex);
            Assert.Equal(column, cell.ColumnIndex);
            Assert.Equal(value, cell.Value);
        }

        [Theory]
        [InlineData(0, 0, -100)]
        [InlineData(0, 80, 20)]
        [InlineData(-1, 0, 0)]
        [InlineData(0, -1, 0)]
        public void NewNumberCell_ThrowArgumentException(int row, int column, int value)
        {
            // Arrange & Act & Assert
            Assert.Throws<ArgumentException>(() => new NumberCell(row, column, value));
        }

        [Theory]
        [InlineData(-99)]
        [InlineData(999)]
        public void ModifyValue(int newValue)
        {
            // Arrange
            var cell = new NumberCell(0, 0, 0);

            // Act
            cell.ModifyValue(newValue);

            // Assert
            Assert.Equal(newValue, cell.Value);
            Assert.Equal(0, cell.RowIndex);
            Assert.Equal(0, cell.ColumnIndex);
        }

        [Theory]
        [InlineData(-100)]
        [InlineData(1000)]
        public void ModifyValue_ThrowArgumentException(int newValue)
        {
            // Arrange
            var cell = new NumberCell(0, 0, 0);

            // Act & Assert
            Assert.Throws<ArgumentException>(() => { cell.ModifyValue(newValue); });
        }
    }
}
