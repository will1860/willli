﻿using System.Collections.Generic;
using Xunit;

namespace SampleSpreadsheet.Tests
{
    public class SpreadsheetTest
    {
        [Fact]
        public void NewSpreadsheet()
        {
            // Arrange & Act
            var spreadsheet = new Spreadsheet<int>();

            // Assert
            Assert.Empty(spreadsheet.Columns);
            Assert.Empty(spreadsheet.Rows);
        }

        [Fact]
        public void AddColumn_Once()
        {
            // Arrange
            var spreadsheet = new Spreadsheet<int>();

            // Act
            var column = spreadsheet.AddColumn();

            // Assert
            Assert.NotEmpty(spreadsheet.Columns);
            Assert.Single(spreadsheet.Columns);
            Assert.Same(column, spreadsheet.Columns[0]);
            Assert.Equal(0, column.Index);
            Assert.Empty(spreadsheet.Rows); // make sure that rows aren't impacted.
        }

        [Fact]
        public void AddColumn_Twice()
        {
            // Arrange
            var spreadsheet = new Spreadsheet<int>();

            // Act
            var firstColumn = spreadsheet.AddColumn();
            var secondColumn = spreadsheet.AddColumn();

            // Assert
            Assert.NotEmpty(spreadsheet.Columns);
            Assert.Equal(2, spreadsheet.Columns.Count);
            Assert.Same(firstColumn, spreadsheet.Columns[0]);
            Assert.Same(secondColumn, spreadsheet.Columns[1]);
            Assert.Equal(0, firstColumn.Index);
            Assert.Equal(1, secondColumn.Index);
            Assert.Empty(spreadsheet.Rows); // make sure that rows aren't impacted.
        }

        [Fact]
        public void AddRow_Once()
        {
            // Arrange
            var spreadsheet = new Spreadsheet<int>();
            var cells = new List<ICell<int>>();

            // Act
            var row = spreadsheet.AddRow(cells);

            // Assert
            Assert.NotEmpty(spreadsheet.Rows);
            Assert.Single(spreadsheet.Rows);
            Assert.Same(row, spreadsheet.Rows[0]);
        }

        [Fact]
        public void AddRow_Twice()
        {
            // Arrange
            var spreadsheet = new Spreadsheet<int>();
            var firstCells = new List<ICell<int>>();
            var secondCells = new List<ICell<int>>();

            // Act
            var firstRow = spreadsheet.AddRow(firstCells);
            var secondRow = spreadsheet.AddRow(secondCells);

            // Assert
            Assert.NotEmpty(spreadsheet.Rows);
            Assert.Equal(2, spreadsheet.Rows.Count);
            Assert.Same(firstRow, spreadsheet.Rows[0]);
            Assert.Same(secondRow, spreadsheet.Rows[1]);
        }
    }
}
